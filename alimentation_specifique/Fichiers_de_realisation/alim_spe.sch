EESchema Schematic File Version 4
LIBS:alim_spe-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Alimentation Spécifique"
Date "2024-02-21"
Rev "1.1"
Comp "I.U.T G.E.I.I. Brive"
Comment1 "PATIENT"
Comment2 "USCHE"
Comment3 "M.PAGE Mme COSTA"
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:LM7805_TO220 U2
U 1 1 65C4E9A0
P 5600 4400
F 0 "U2" H 5600 4642 50  0000 C CNN
F 1 "LM7805_TO220" H 5600 4551 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 5600 4625 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 5600 4350 50  0001 C CNN
	1    5600 4400
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM7809_TO220 U1
U 1 1 65C4EA29
P 5600 2900
F 0 "U1" H 5600 3142 50  0000 C CNN
F 1 "LM7809_TO220" H 5600 3051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 5600 3125 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 5600 2850 50  0001 C CNN
	1    5600 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 65C4EBF3
P 6800 4600
F 0 "R2" H 6650 4650 50  0000 C CNN
F 1 "330" H 6650 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6730 4600 50  0001 C CNN
F 3 "~" H 6800 4600 50  0001 C CNN
	1    6800 4600
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 65C4ED7E
P 6800 3450
F 0 "D1" V 6750 3200 50  0000 C CNN
F 1 "Green" V 6850 3200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric" H 6800 3450 50  0001 C CNN
F 3 "~" H 6800 3450 50  0001 C CNN
	1    6800 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 65C4EDFA
P 6800 4950
F 0 "D2" V 6750 4700 50  0000 C CNN
F 1 "Red" V 6850 4700 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric" H 6800 4950 50  0001 C CNN
F 3 "~" H 6800 4950 50  0001 C CNN
	1    6800 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 3200 6200 3200
Wire Wire Line
	4850 3200 5600 3200
Connection ~ 5600 3200
Wire Wire Line
	5600 4700 6200 4700
Wire Wire Line
	4850 4700 5600 4700
Connection ~ 5600 4700
Wire Wire Line
	5900 4400 6200 4400
$Comp
L Device:CP1 C4
U 1 1 65C50103
P 6200 4550
F 0 "C4" H 6315 4596 50  0000 L CNN
F 1 "0.33µF" H 6315 4505 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.8" H 6200 4550 50  0001 C CNN
F 3 "~" H 6200 4550 50  0001 C CNN
	1    6200 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 65C5014F
P 4850 4550
F 0 "C2" H 4965 4596 50  0000 L CNN
F 1 "0.33µF" H 4965 4505 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.8" H 4850 4550 50  0001 C CNN
F 3 "~" H 4850 4550 50  0001 C CNN
	1    4850 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 65C506CE
P 4850 3050
F 0 "C1" H 4965 3096 50  0000 L CNN
F 1 "0.33µF" H 4965 3005 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.8" H 4850 3050 50  0001 C CNN
F 3 "~" H 4850 3050 50  0001 C CNN
	1    4850 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2900 6200 2900
$Comp
L Device:CP1 C3
U 1 1 65C50945
P 6200 3050
F 0 "C3" H 6315 3096 50  0000 L CNN
F 1 "0.33µF" H 6315 3005 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.8" H 6200 3050 50  0001 C CNN
F 3 "~" H 6200 3050 50  0001 C CNN
	1    6200 3050
	1    0    0    -1  
$EndComp
Connection ~ 6200 2900
$Comp
L power:GND #PWR06
U 1 1 65C509EE
P 5600 4700
F 0 "#PWR06" H 5600 4450 50  0001 C CNN
F 1 "GND" H 5605 4527 50  0000 C CNN
F 2 "" H 5600 4700 50  0001 C CNN
F 3 "" H 5600 4700 50  0001 C CNN
	1    5600 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 65C50A24
P 5600 3200
F 0 "#PWR05" H 5600 2950 50  0001 C CNN
F 1 "GND" H 5605 3027 50  0000 C CNN
F 2 "" H 5600 3200 50  0001 C CNN
F 3 "" H 5600 3200 50  0001 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 65C51770
P 3650 3150
F 0 "J1" H 3800 3150 50  0000 C CNN
F 1 "Bornier_12V" H 3700 3250 50  0000 C CNN
F 2 "Connector_JST:JST_NV_B02P-NV_1x02_P5.00mm_Vertical" H 3650 3150 50  0001 C CNN
F 3 "~" H 3650 3150 50  0001 C CNN
	1    3650 3150
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 65C5248F
P 3850 4250
F 0 "#PWR02" H 3850 4000 50  0001 C CNN
F 1 "GND" H 3855 4077 50  0000 C CNN
F 2 "" H 3850 4250 50  0001 C CNN
F 3 "" H 3850 4250 50  0001 C CNN
	1    3850 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 65C530D2
P 7700 2900
F 0 "J3" H 7650 3000 50  0000 L CNN
F 1 "+9V" H 7600 2700 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B02P-NV_1x02_P5.00mm_Vertical" H 7700 2900 50  0001 C CNN
F 3 "~" H 7700 2900 50  0001 C CNN
	1    7700 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 65C554BD
P 7500 3000
F 0 "#PWR07" H 7500 2750 50  0001 C CNN
F 1 "GND" H 7500 2850 50  0000 C CNN
F 2 "" H 7500 3000 50  0001 C CNN
F 3 "" H 7500 3000 50  0001 C CNN
	1    7500 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0101
U 1 1 65C583B2
P 3850 2800
F 0 "#PWR0101" H 3850 2650 50  0001 C CNN
F 1 "+12V" H 3750 2950 50  0000 C CNN
F 2 "" H 3850 2800 50  0001 C CNN
F 3 "" H 3850 2800 50  0001 C CNN
	1    3850 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0102
U 1 1 65C583F0
P 4950 2900
F 0 "#PWR0102" H 4950 2750 50  0001 C CNN
F 1 "+12V" H 4965 3073 50  0000 C CNN
F 2 "" H 4950 2900 50  0001 C CNN
F 3 "" H 4950 2900 50  0001 C CNN
	1    4950 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0103
U 1 1 65C5842E
P 4950 4400
F 0 "#PWR0103" H 4950 4250 50  0001 C CNN
F 1 "+12V" H 4965 4573 50  0000 C CNN
F 2 "" H 4950 4400 50  0001 C CNN
F 3 "" H 4950 4400 50  0001 C CNN
	1    4950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2900 4950 2900
Wire Wire Line
	4850 4400 4950 4400
Connection ~ 4950 4400
Wire Wire Line
	4950 4400 5300 4400
Connection ~ 4950 2900
Wire Wire Line
	4950 2900 5300 2900
Wire Wire Line
	3850 2800 3850 3050
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 65C5B0F9
P 4100 2800
F 0 "#FLG0101" H 4100 2875 50  0001 C CNN
F 1 "PWR_FLAG" H 4150 2950 50  0000 C CNN
F 2 "" H 4100 2800 50  0001 C CNN
F 3 "~" H 4100 2800 50  0001 C CNN
	1    4100 2800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 65C5B4A9
P 3600 4200
F 0 "#FLG0102" H 3600 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 4374 50  0000 C CNN
F 2 "" H 3600 4200 50  0001 C CNN
F 3 "~" H 3600 4200 50  0001 C CNN
	1    3600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4200 3850 4200
Wire Wire Line
	3850 4200 3850 4250
Connection ~ 6200 4400
Wire Wire Line
	6200 2900 6800 2900
$Comp
L Device:R R1
U 1 1 65C779BE
P 6800 3100
F 0 "R1" H 6650 3150 50  0000 C CNN
F 1 "820" H 6650 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 6730 3100 50  0001 C CNN
F 3 "~" H 6800 3100 50  0001 C CNN
	1    6800 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 2950 6800 2900
$Comp
L power:GND #PWR0104
U 1 1 65C79D2A
P 6800 3650
F 0 "#PWR0104" H 6800 3400 50  0001 C CNN
F 1 "GND" H 6805 3477 50  0000 C CNN
F 2 "" H 6800 3650 50  0001 C CNN
F 3 "" H 6800 3650 50  0001 C CNN
	1    6800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3300 6800 3250
Wire Wire Line
	6800 3600 6800 3650
Wire Wire Line
	6800 4450 6800 4400
Wire Wire Line
	6200 4400 6800 4400
Connection ~ 6800 4400
Wire Wire Line
	6800 4750 6800 4800
Wire Wire Line
	6800 5100 6800 5150
$Comp
L power:GND #PWR0105
U 1 1 65C7E06B
P 6800 5150
F 0 "#PWR0105" H 6800 4900 50  0001 C CNN
F 1 "GND" H 6805 4977 50  0000 C CNN
F 2 "" H 6800 5150 50  0001 C CNN
F 3 "" H 6800 5150 50  0001 C CNN
	1    6800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4400 7550 4400
Connection ~ 6800 2900
Wire Wire Line
	6800 2900 7500 2900
Wire Wire Line
	3850 2800 4100 2800
Connection ~ 3850 2800
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 65C782A9
P 4050 3700
F 0 "J2" H 4129 3742 50  0000 L CNN
F 1 "GND" H 4129 3651 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 4050 3700 50  0001 C CNN
F 3 "~" H 4050 3700 50  0001 C CNN
	1    4050 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 65C782EF
P 7800 4400
F 0 "J4" H 7750 4600 50  0000 L CNN
F 1 "+5V" H 7700 4200 50  0000 L CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 7800 4400 50  0001 C CNN
F 3 "~" H 7800 4400 50  0001 C CNN
	1    7800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 4300 7550 4300
Wire Wire Line
	7550 4300 7550 4400
Wire Wire Line
	7550 4400 7550 4500
Wire Wire Line
	7550 4500 7600 4500
Connection ~ 7550 4400
Wire Wire Line
	7550 4400 7600 4400
Wire Wire Line
	3850 3150 3850 3600
Connection ~ 3850 4200
Connection ~ 3850 3600
Wire Wire Line
	3850 3600 3850 3700
Connection ~ 3850 3700
Wire Wire Line
	3850 3700 3850 3800
Connection ~ 3850 3800
Wire Wire Line
	3850 3800 3850 4200
Wire Notes Line
	4650 2600 7950 2600
Wire Notes Line
	7950 2600 7950 3900
Wire Notes Line
	7950 3900 4650 3900
Wire Notes Line
	4650 3900 4650 2600
Text Notes 4650 3900 0    50   ~ 10
Regulation of 12V DC into 9V DC
Wire Notes Line
	7950 4150 4650 4150
Wire Notes Line
	4650 4150 4650 5400
Wire Notes Line
	4650 5400 7950 5400
Wire Notes Line
	7950 5400 7950 4150
Text Notes 4650 5400 0    50   ~ 10
Regulation of 12V DC into 5V DC
Wire Notes Line
	7400 2700 7400 3250
Wire Notes Line
	7400 3250 7950 3250
Wire Notes Line
	7400 2700 7950 2700
Wire Notes Line
	7350 4150 7350 4700
Wire Notes Line
	7350 4700 7950 4700
Text Notes 7350 4700 0    50   ~ 10
Output voltage
Text Notes 7400 3250 0    50   ~ 10
Output voltage
Wire Notes Line
	4400 2500 4400 3450
Wire Notes Line
	4400 3450 3300 3450
Wire Notes Line
	3300 3450 3300 2500
Wire Notes Line
	3300 2500 4400 2500
Text Notes 3900 3450 0    50   ~ 10
Input voltage
$EndSCHEMATC
